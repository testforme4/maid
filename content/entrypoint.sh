#!/bin/sh

RESTORE_BACKUP() {
    BACKUP=$(curl -4 --retry 4 https://${CLOUDFLARE_WORKERS_HOST}/backup?key=${CLOUDFLARE_WORKERS_KEY} | jq .value)
    DIR_TMP="$(mktemp -d)"
    echo ${BACKUP} | base64 -d >${DIR_TMP}/backup.tar.gz
    tar -zxf ${DIR_TMP}/backup.tar.gz -C /pagermaid/workdir
    rm -rf ${DIR_TMP}
}

WORKER_STATUS=$(curl -4 --retry 4 https://${CLOUDFLARE_WORKERS_HOST} | jq .status | sed "s|\"||g")

if [ "${WORKER_STATUS}" = "Running" ]; then
    RESTORE_BACKUP
else
    echo "无法连接 Cloudflare Workers"
    touch /workdir/workers_fail.lock
fi

exec runsvdir -P /etc/service
