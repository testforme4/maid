FROM teampgm/pagermaid_pyro

COPY ./content /workdir

RUN apt-get update && apt-get install -y \
    ttyd \
    runit \
    nano \
    && sh /workdir/install.sh \
    && rm -rf /var/lib/apt/lists/* \
    && chmod +x /workdir/service/*/run \
    && ln -s /workdir/service/* /etc/service

ENV PORT=3000

EXPOSE 3000

ENTRYPOINT ["runsvdir", "-P", "/etc/service"]

